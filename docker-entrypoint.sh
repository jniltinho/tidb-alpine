#!/bin/sh

# parameters
USQL="/usr/local/bin/usql mysql://root:@127.0.0.1:4000"
PWGEN=$(/usr/local/bin/pwgen -l=32 -c=1)


if [ "$1" = '/bin/sh' ]; then
    exec /bin/sh
    exit 1
fi

if [ ! -z "$MYSQL_RANDOM_ROOT_PASSWORD" ]; then
    export MYSQL_ROOT_PASSWORD=$PWGEN
    echo "GENERATED ROOT PASSWORD: $MYSQL_ROOT_PASSWORD"
fi


if [ -z "$MYSQL_ROOT_PASSWORD" -a -z "$MYSQL_RANDOM_ROOT_PASSWORD" ]; then
    echo >&2 'error: database is uninitialized and password option is not specified '
    echo >&2 '   You need to specify one of MYSQL_ROOT_PASSWORD and MYSQL_RANDOM_ROOT_PASSWORD'
    exit 1
fi



if [ -d /var/lib/tidb ]; then
    echo '[i] TiDB directory already present, skipping creation'
else
    echo "[i] TiDB data directory not found, creating initial DBs"
    
    # init database
    echo '[i] Initializing database'
    #/tidb-server -P=4000 -path=/var/lib/tidb >/dev/null &
    /tidb-server -socket=/tmp/tidb.sock -host=127.0.0.1 -path=/var/lib/tidb >/dev/null &
    sleep 3
    $USQL -c "SHOW DATABASES;"
    echo 'Database initialized'
    
    
    # Create new database
    if [ "$MYSQL_DATABASE" ]; then
        echo "[i] Creating database: $MYSQL_DATABASE"
        $USQL -c "CREATE DATABASE IF NOT EXISTS $MYSQL_DATABASE CHARACTER SET utf8 COLLATE utf8_general_ci;"
        
        # set new User and Password
        if [ "$MYSQL_USER" -a "$MYSQL_PASSWORD" ]; then
            echo "[i] Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
            $USQL -c "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';"
            $USQL -c "GRANT ALL ON $MYSQL_DATABASE.* to '$MYSQL_USER'@'%';"
        fi
    else
        # don`t need to create new database,Set new User to control all database.
        if [ "$MYSQL_USER" -a "$MYSQL_PASSWORD" ]; then
            echo "[i] Creating user: $MYSQL_USER with password $MYSQL_PASSWORD"
            $USQL -c "CREATE USER '$MYSQL_USER'@'%' IDENTIFIED BY '$MYSQL_PASSWORD';"
            $USQL -c "GRANT ALL ON *.* to '$MYSQL_USER'@'%';"
            
        fi
    fi
    
    # run sql
    echo "[i] Set root password: $MYSQL_ROOT_PASSWORD"
    $USQL -c "DROP DATABASE IF EXISTS test;"
    $USQL -c "SET PASSWORD FOR 'root'@'%' = '$MYSQL_ROOT_PASSWORD';"
    kill -9 $(pidof tidb-server)
    rm -f /tmp/tidb.sock
fi

echo "[i] Sleeping 3 sec"
sleep 3

echo '[i] Start running TiDB'
exec /usr/local/bin/dumb-init /tidb-server -P=3306 -path=/var/lib/tidb "$@"
