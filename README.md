# TiDB Alpine

Build TiDB on Alpine Docker Image, run like MySQL image.<br />
TiDB is a distributed HTAP database compatible with the MySQL protocol.

## Run TiDB Docker Image

```bash
docker run -d -e MYSQL_RANDOM_ROOT_PASSWORD='yes' -e MYSQL_USER='nilton_sys' \
 -e MYSQL_PASSWORD='123456' -e MYSQL_DATABASE='nilton' -e TZ='America/Sao_Paulo' -p 3306:3306 \
 --name tidb-alpine-server jniltinho/tidb-alpine
```

## Alpine default VARs

```bash
## Config tzdata example TZ='America/Sao_Paulo'
TZ
```

## What is TiDB?

TiDB ("Ti" stands for Titanium) is an open-source NewSQL database that supports Hybrid Transactional and Analytical Processing (HTAP) workloads. It is MySQL compatible and features horizontal scalability, strong consistency, and high availability.

- __Horizontal Scalability__

    TiDB expands both SQL processing and storage by simply adding new nodes. This makes infrastructure capacity planning both easier and more cost-effective than traditional relational databases which only scale vertically.

- __MySQL Compatible Syntax__

    TiDB acts like it is a MySQL 5.7 server to your applications. You can continue to use all of the existing MySQL client libraries, and in many cases, you will not need to change a single line of code in your application. Because TiDB is built from scratch, not a MySQL fork, please check out the list of [known compatibility differences](./sql/mysql-compatibility.md).

- __High Availability__

    TiDB uses the Raft consensus algorithm to ensure that data is highly available and safely replicated throughout storage in Raft groups. In the event of failure, a Raft group will automatically elect a new leader for the failed member, and self-heal the TiDB cluster without any required manual intervention. Failure and self-healing operations are also transparent to applications.

## Links

- [**Stack Overflow**](https://stackoverflow.com/questions/tagged/tidb)
- [**Blog**](https://www.pingcap.com/blog/)
- [**GitHub**](https://github.com/pingcap/tidb)
- [**DockerHub**](https://hub.docker.com/r/jniltinho/tidb-alpine)