# Builder image
FROM golang:1.12-alpine as builder
LABEL maintainer="Nilton OS <jniltinho@gmail.com>"

# docker tag tidb-alpine docker.io/jniltinho/tidb-alpine
# docker push docker.io/jniltinho/tidb-alpine

# docker run --name tidb-alpine-server -d -p 3306:3306 jniltinho/tidb-alpine:latest
# docker run -d -e MYSQL_RANDOM_ROOT_PASSWORD='yes' -e MYSQL_USER='nilton_sys' \
# -e MYSQL_PASSWORD='123456' -e MYSQL_DATABASE='nilton' -p 3306:3306 \
# --name tidb-alpine-server jniltinho/tidb-alpine

# mysql -h 127.0.0.1 -P 3306 -u nilton_sys -p -D nilton
# mysql -h 127.0.0.1 -P 3306 < file.sql


# docker stop $(docker ps -qa)
# docker rm -f $(docker ps -qa)
# docker rmi $(docker images -qa)
# docker stop $(docker ps -a|grep tidb-alpine|awk '{print $1}')
# docker rm -f $(docker ps -a|grep tidb-alpine|awk '{print $1}')


# docker build -t tidb-alpine .

RUN apk add --no-cache upx curl make git gcc musl-dev

#ENV TZ=America/Sao_Paulo

RUN curl -skL https://github.com/Yelp/dumb-init/releases/download/v1.2.2/dumb-init_1.2.2_amd64 -o /dumb-init \
    && chmod +x /dumb-init \
    && mkdir -p /go/src/github.com/pingcap \
    && cd /go/src/github.com/pingcap \
    && git clone https://github.com/pingcap/tidb.git


RUN go get -ldflags "-s -w" -u github.com/xo/usql \
    && upx $GOPATH/bin/usql


WORKDIR /go/src/github.com/pingcap/tidb/

RUN make \
    && upx $GOPATH/src/github.com/pingcap/tidb/bin/tidb-server    


RUN go get github.com/jniltinho/go-pwgen \
    && cd $GOPATH/src/github.com/jniltinho/go-pwgen/ \
    && GO111MODULE=on go build -ldflags "-s -w" -v -o /usr/local/bin/pwgen cmd/pw/*.go \
    && upx /usr/local/bin/pwgen


# Executable image
FROM alpine

RUN apk add --no-cache tzdata && rm -rf /var/cache/apk/*

COPY --from=builder /go/src/github.com/pingcap/tidb/bin/tidb-server /tidb-server
COPY --from=builder /dumb-init /usr/local/bin/dumb-init
COPY --from=builder /go/bin/usql /usr/local/bin/usql
COPY --from=builder /usr/local/bin/pwgen /usr/local/bin/pwgen
#COPY --from=builder /usr/share/zoneinfo/America/Sao_Paulo /etc/localtime

WORKDIR /

EXPOSE 3306

COPY ./docker-entrypoint.sh /
RUN chmod +x /docker-entrypoint.sh
ENTRYPOINT ["/docker-entrypoint.sh"]

#CMD ["/usr/local/bin/dumb-init", "/tidb-server", "-P", "3306", "-path", "/var/lib/tidb"]
